#include "godotchartboost.h"
#include "core/globals.h"
#include "core/variant.h"
#include "core/message_queue.h"
#import <Chartboost/Chartboost.h>

//GodotChartboost* GodotChartboost::instance = NULL;
GodotChartboost* instance = NULL;

@interface ChartboostDelegateBridge : NSObject<ChartboostDelegate>



@end

static ChartboostDelegateBridge* s_pDelegateBridge = nil;

int GodotChartboost::get_call_id()
{
    return call_id;
}

void GodotChartboost::init(int call_id /*String app_id, String app_signature*/)
{
    if(!initialized)
    {
        this->call_id = call_id;
        String app_id = Globals::get_singleton()->get("chartboost/id");
        String app_signature = Globals::get_singleton()->get("chartboost/signature");
        
        NSString *nsAppId = [NSString stringWithCString:app_id.utf8().get_data() encoding:NSUTF8StringEncoding];
        NSString *nsAppSignature = [NSString stringWithCString:app_signature.utf8().get_data() encoding:NSUTF8StringEncoding];
        s_pDelegateBridge = [[ChartboostDelegateBridge alloc] init];
        [Chartboost startWithAppId:nsAppId
                    appSignature:nsAppSignature
                    delegate:s_pDelegateBridge];
        initialized = true;
    }
};

void GodotChartboost::cacheInterstitial(const String& location)
{
    if(initialized)
    {
        NSString *nsLocation = [NSString stringWithCString:location.utf8().get_data() encoding:NSUTF8StringEncoding];
        [Chartboost cacheInterstitial:nsLocation];
    }
};

bool GodotChartboost::hasInterstitial(const String& location)
{
    bool result = false;
    if (initialized)
    {
        NSString *nsLocation = [NSString stringWithCString:location.utf8().get_data() encoding:NSUTF8StringEncoding];
        result = [Chartboost hasInterstitial:nsLocation];
    }
    return result;
};

void GodotChartboost::showInterstitial(const String& location)
{
    if(initialized)
    {
        NSString *nsLocation = [NSString stringWithCString:location.utf8().get_data() encoding:NSUTF8StringEncoding];
        [Chartboost showInterstitial:nsLocation];
    }
};

void GodotChartboost::cacheMoreApps(const String& location)
{
    if(initialized)
    {
        NSString *nsLocation = [NSString stringWithCString:location.utf8().get_data() encoding:NSUTF8StringEncoding];
        NSLog(@"Cache interstitial");
        [Chartboost cacheMoreApps:nsLocation];
    }
};

bool GodotChartboost::hasMoreApps(const String& location)
{
    bool result = false;
    if (initialized)
    {
        NSString *nsLocation = [NSString stringWithCString:location.utf8().get_data() encoding:NSUTF8StringEncoding];
        result = [Chartboost hasMoreApps:nsLocation];
    }
    return result;
};

void GodotChartboost::showMoreApps(const String& location)
{
    if(initialized)
    {
        NSString *nsLocation = [NSString stringWithCString:location.utf8().get_data() encoding:NSUTF8StringEncoding];
        [Chartboost showMoreApps:nsLocation];
    }
};

void GodotChartboost::cacheRewardVideo(const String& location)
{
    if(initialized)
    {
        NSString *nsLocation = [NSString stringWithCString:location.utf8().get_data() encoding:NSUTF8StringEncoding];
        [Chartboost cacheRewardedVideo:nsLocation];
    }
};

bool GodotChartboost::hasRewardVideo(const String& location)
{
    bool result = false;
    if (initialized)
    {
        NSString *nsLocation = [NSString stringWithCString:location.utf8().get_data() encoding:NSUTF8StringEncoding];
        result = [Chartboost hasRewardedVideo:nsLocation];
    }
    return result;
};

void GodotChartboost::showRewardVideo(const String& location)
{
    if(initialized)
    {
        NSString *nsLocation = [NSString stringWithCString:location.utf8().get_data() encoding:NSUTF8StringEncoding];
        [Chartboost showRewardedVideo:nsLocation];
    }
};

void GodotChartboost::setAutoCacheAds(bool autoCacheAds)
{
    if(initialized)
    {
        [Chartboost setAutoCacheAds:autoCacheAds];
    }
};

bool GodotChartboost::getAutoCacheAds()
{
    bool autoCacheAds = false;
    if(initialized)
    {
        autoCacheAds = [Chartboost getAutoCacheAds];
    }
    return autoCacheAds;
};

void GodotChartboost::setShouldRequestInterstitialsInFirstSession(bool shouldRequest)
{
    if(initialized)
    {
        [Chartboost setShouldRequestInterstitialsInFirstSession:shouldRequest];
    }
};

bool GodotChartboost::isAnyViewVisible()
{
    if(initialized)
    {
        return [Chartboost isAnyViewVisible];
    }
    
    return false;
}

void GodotChartboost::closeImpression()
{
    if(initialized)
    {
        [Chartboost closeImpression];
    }

}

void GodotChartboost::_bind_methods()
{
    ObjectTypeDB::bind_method(_MD("init"),                  &GodotChartboost::init);
    ObjectTypeDB::bind_method(_MD("cacheInterstitial"),    &GodotChartboost::cacheInterstitial);
    ObjectTypeDB::bind_method(_MD("hasInterstitial"),      &GodotChartboost::hasInterstitial);
    ObjectTypeDB::bind_method(_MD("showInterstitial"),     &GodotChartboost::showInterstitial);
    ObjectTypeDB::bind_method(_MD("cacheMoreApps"),       &GodotChartboost::cacheMoreApps);
    ObjectTypeDB::bind_method(_MD("hasMoreApps"),         &GodotChartboost::hasMoreApps);
    ObjectTypeDB::bind_method(_MD("showMoreApps"),        &GodotChartboost::showMoreApps);
    ObjectTypeDB::bind_method(_MD("cacheRewardedVideo"),    &GodotChartboost::cacheRewardVideo);
    ObjectTypeDB::bind_method(_MD("hasRewardedVideo"),      &GodotChartboost::hasRewardVideo);
    ObjectTypeDB::bind_method(_MD("showRewardedVideo"),     &GodotChartboost::showRewardVideo);
    ObjectTypeDB::bind_method(_MD("getAutoCacheAds"),    &GodotChartboost::getAutoCacheAds);
    ObjectTypeDB::bind_method(_MD("setAutoCacheAds"),    &GodotChartboost::setAutoCacheAds);
    ObjectTypeDB::bind_method(_MD("set_should_request_interstitials_in_first_session"),     &GodotChartboost::setShouldRequestInterstitialsInFirstSession);
    
    ObjectTypeDB::bind_method(_MD("isAnyViewVisible"),    &GodotChartboost::isAnyViewVisible);
    ObjectTypeDB::bind_method(_MD("closeImpression"),    &GodotChartboost::closeImpression);
};


GodotChartboost::GodotChartboost()
{
    ERR_FAIL_COND(instance != NULL);
    instance = this;
    initialized = false;
};

GodotChartboost::~GodotChartboost()
{
    instance = NULL;
}

@implementation ChartboostDelegateBridge

- (void)didCompleteRewardedVideo:(CBLocation)location
                      withReward:(int)reward
{
    if (instance){
        String loc = [location cStringUsingEncoding:NSUTF8StringEncoding];
        MessageQueue::get_singleton()->push_call(instance->get_call_id(), "didCompleteRewardedVideo", Variant(loc), Variant(reward));
    }
}

@end
